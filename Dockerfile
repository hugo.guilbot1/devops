FROM ubuntu:22.04

RUN useradd -ms /bin/bash gitlab-runner

# Install dependencies
RUN apt update -y && apt upgrade -y && \
    apt install -y python3.10 python3-pip vim && \
    rm -rf /var/lib/apt/lists/*

USER gitlab-runner

WORKDIR /home/gitlab-runner

ENV PATH="/home/gitlab-runner/.local/bin:${PATH}"

COPY requirements-azure.txt /home/gitlab-runner/requirements-azure.txt

RUN pip3 install --upgrade pip && \
    pip3 install ansible-core==2.14.6 ansible-lint ansible"[azure]" && \
    pip3 install -r requirements-azure.txt && rm requirements-azure.txt

